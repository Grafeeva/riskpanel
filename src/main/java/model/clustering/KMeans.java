package model.clustering;

import javafx.collections.ObservableList;
import javafx.collections.ObservableSet;
import model.TableData;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kate on 10.11.2015.
 */
public class KMeans {
    private int numberOfClusters;
    private List<Cluster> clusters;
    private ObservableList<TableData> data;

    public KMeans(int numberOfClusters, ObservableList<TableData> data) {
        this.numberOfClusters = numberOfClusters;
        clusters = new ArrayList<>();
        this.data = data;

        initCenters();

        clustering();
        System.out.print("f");
    }

    public void initCenters() {

            clusters.add(new Cluster(new Center(new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 0.0})));
        clusters.add(new Cluster(new Center(new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 1.0})));
        clusters.add(new Cluster(new Center(new double[] {0.0, 0.0, 0.0, 0.0, 1.0, 0.0})));
        clusters.add(new Cluster(new Center(new double[] {0.0, 0.0, 0.0, 1.0, 0.0, 0.0})));
        clusters.add(new Cluster(new Center(new double[] {0.0, 0.0, 1.0, 0.0, 0.0, 0.0})));
        clusters.add(new Cluster(new Center(new double[] {0.0, 1.0, 0.0, 0.0, 0.0, 0.0})));
        clusters.add(new Cluster(new Center(new double[] {1.0, 0.0, 0.0, 0.0, 0.0, 0.0})));
        clusters.add(new Cluster(new Center(new double[] {1.0, 1.0, 1.0, 0.0, 0.0, 0.0})));
    }

    public void clustering() {
        for(TableData tableData: data) {
            double minDist = dist(tableData, clusters.get(0).getCenter());
            int cluster = 0;
            for(int j = 1; j < numberOfClusters; j++) {
                double dist = dist(tableData, clusters.get(j).getCenter());
                if(dist < minDist) {
                    minDist = dist;
                    cluster = j;
                }
            }

            clusters.get(cluster).addPrecedent(tableData);


            for(int j = 0; j < numberOfClusters; j++) {
                int countOfPrecedent = clusters.get(j).getData().size();
                double totalConsequence = 0;
                double totalLoss = 0;
                double totalMeasure = 0;
                double totalSymptom = 0;
                double totalThreat = 0;
                double totalVulnerability = 0;

                for(TableData data : clusters.get(j).getData()) {
                    totalConsequence += data.getConsequenceUncertain();
                    totalLoss += data.getLossUncertain();
                    totalMeasure += data.getMeasureUncertain();
                    totalSymptom += data.getSymptomUncertain();
                    totalThreat += data.getThreatUncertain();
                    totalVulnerability += data.getVulnerabilityUncertain();
                }
                if(countOfPrecedent != 0) {
                clusters.get(j).setCenter(new Center(new double[]{totalLoss/(double)countOfPrecedent, totalThreat/(double)countOfPrecedent, totalMeasure/(double)countOfPrecedent, totalVulnerability/(double)countOfPrecedent, totalConsequence/(double)countOfPrecedent, totalSymptom/(double)countOfPrecedent}));}
            }
        }

        boolean isStillMoving = true;

        while (isStillMoving) {
            for(int j = 0; j < numberOfClusters; j++) {
                int countOfPrecedent = clusters.get(j).getData().size();
                double totalConsequence = 0;
                double totalLoss = 0;
                double totalMeasure = 0;
                double totalSymptom = 0;
                double totalThreat = 0;
                double totalVulnerability = 0;

                for(TableData data : clusters.get(j).getData()) {
                    totalConsequence += data.getConsequenceUncertain();
                    totalLoss += data.getLossUncertain();
                    totalMeasure += data.getMeasureUncertain();
                    totalSymptom += data.getSymptomUncertain();
                    totalThreat += data.getThreatUncertain();
                    totalVulnerability += data.getVulnerabilityUncertain();
                }

                clusters.get(j).setCenter(new Center(new double[]{totalLoss/(double)countOfPrecedent, totalThreat/(double)countOfPrecedent, totalMeasure/(double)countOfPrecedent, totalVulnerability/(double)countOfPrecedent, totalConsequence/(double)countOfPrecedent, totalSymptom/(double)countOfPrecedent}));
            }

            isStillMoving = false;

            for(TableData tableData: data) {


                double minDist = dist(tableData, clusters.get(0).getCenter());
                int cluster = 0;
                for(int j = 1; j < numberOfClusters; j++) {
                    double dist = dist(tableData, clusters.get(j).getCenter());
                    if(dist < minDist) {
                        minDist = dist;
                        cluster = j;
                    }
                }

                for(Cluster cluster1: clusters) {
                    if(cluster1.contains(tableData) && cluster1 != clusters.get(cluster)) {
                        cluster1.removePrecedent(tableData);
                        clusters.get(cluster).addPrecedent(tableData);
                        isStillMoving = true;
                    }
                }
            }
        }
    }

    private double dist(TableData data, Center center) {
        return Math.sqrt(Math.pow(data.getConsequenceUncertain() - center.getConsequenceUncertain(), 2) + Math.pow(data.getLossUncertain() - center.getLossUncertain(), 2) +
                Math.pow(data.getMeasureUncertain() - center.getMeasureUncertain(), 2) + Math.pow(data.getSymptomUncertain() - center.getSymptomUncertain(), 2) +
                Math.pow(data.getThreatUncertain() - center.getThreatUncertain(), 2) + Math.pow(data.getVulnerabilityUncertain() - center.getVulnerabilityUncertain(), 2)
        );
    }

    public void print() {
        for(int i = 0; i < clusters.size(); i++) {
            System.out.println("cluster " + i + " " + clusters.get(i).getData().size());
        }
    }
}
