package model;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;
import org.neo4j.graphdb.factory.GraphDatabaseSettings;
import view.App;

import java.util.*;

public class Model {
    private static volatile Model instance;

    public static final String DEFAULT_PATH = "C:\\Education\\Projects\\ВКР\\neo4j-riskpanel";

    private GraphDatabaseService dbService;

    private List<Tree> attributes;
    private ObservableList<TableData> tableData = FXCollections.observableArrayList();
    private Map<TableData, Integer> equalsCount = new HashMap<>();
    private int countPrecedent = 0;
    private StringProperty countPrecedentProperty = new SimpleStringProperty();
    private int countPrecedentWithoutRelationships = 0;
    private StringProperty countPrecedentWithoutRelationshipsProperty = new SimpleStringProperty();

    private ObservableList<Precedent> emptyPrecedents = FXCollections.observableArrayList();
    public  List<Precedent> removedPrecedents = new ArrayList<>();

    private Model(){
        attributes = new ArrayList<>();
    }

    public static Model getInstance() {
        Model localInstance = instance;
        if(localInstance == null) {
            synchronized (Model.class) {
                if(localInstance == null) {
                    instance = localInstance = new Model();
                }
            }
        }
        return instance;
    }

    public ObservableList<Precedent> getEmptyPrecedents() {
        return emptyPrecedents;
    }

    public GraphDatabaseService getDbService() {
        return dbService;
    }

    public ObservableList<TableData> getTableData() {
        return tableData;
    }

    public void connectToDB(String path) {
        if(dbService != null) closeConnection();
        dbService = new GraphDatabaseFactory().newEmbeddedDatabaseBuilder(path).setConfig(GraphDatabaseSettings.allow_store_upgrade, "true")
                .newGraphDatabase();;
    }


    public List<Tree> getAttributes() {
        return attributes;
    }

    public int getCountPrecedent() {
        return countPrecedent;
    }

    public int getCountPrecedentWithoutRelationships() {
        return countPrecedentWithoutRelationships;
    }

    public String getCountPrecedentWithoutRelationshipsProperty() {
        return countPrecedentWithoutRelationshipsProperty.get();
    }

    public StringProperty countPrecedentWithoutRelationshipsPropertyProperty() {
        return countPrecedentWithoutRelationshipsProperty;
    }

    public void addTreeToAttr(Tree tree) {
        attributes.add(tree);
    }

    public void countPrecedentInc() {
        countPrecedent++;
        countPrecedentProperty.setValue(String.valueOf(countPrecedent));
    }

    public String getCountPrecedentProperty() {
        return countPrecedentProperty.get();
    }

    public StringProperty countPrecedentPropertyProperty() {
        return countPrecedentProperty;
    }

    public void countPrecedentWithoutRelationshipsInc(Precedent data) {
        if (!emptyPrecedents.contains(data)) {
            emptyPrecedents.add(data);
        }
        countPrecedentWithoutRelationships++;
        countPrecedentWithoutRelationshipsProperty.setValue(String.valueOf(countPrecedentWithoutRelationships));
    }

    private static void registerShutdownHook(final GraphDatabaseService graphDb) {
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                graphDb.shutdown();
            }
        });
    }

    public void addItem(TableData itemData) {
        Integer count = equalsCount.get(itemData);
        System.out.println(count);
        if(count == null) {
            count = 0;
        }
        equalsCount.put(itemData, count + 1);

        tableData.add(itemData);

    }

    public Map<TableData, Integer> getEqualsCount() {
        return equalsCount;
    }

    public void setEqualsCount(Map<TableData, Integer> equalsCount) {
        this.equalsCount = equalsCount;
    }

    public void closeConnection() {
        if(dbService != null) {
            dbService.shutdown();
        }

        clear();
    }

    public void clear() {
        attributes = new ArrayList<>();
        tableData.clear();
        equalsCount.clear();
        countPrecedent = 0;
        countPrecedentWithoutRelationships = 0;
        countPrecedentWithoutRelationshipsProperty.setValue(String.valueOf(countPrecedentWithoutRelationships));
    }
}
