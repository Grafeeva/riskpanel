package model.clustering;

import javafx.beans.property.DoubleProperty;
import model.TableData;

/**
 * Created by Kate on 10.11.2015.
 */
public class Center {
    private double lossUncertain;
    private double threatUncertain;
    private double measureUncertain;
    private double vulnerabilityUncertain;
    private double consequenceUncertain;
    private double symptomUncertain;

    public Center(TableData data) {
        lossUncertain = data.getLossUncertain();
        threatUncertain = data.getLossUncertain();
        measureUncertain = data.getMeasureUncertain();
        vulnerabilityUncertain = data.getVulnerabilityUncertain();
        consequenceUncertain = data.getConsequenceUncertain();
        symptomUncertain = data.getSymptomUncertain();
    }

    public Center(double []values) {
        lossUncertain = values[0];
        threatUncertain = values[1];
        measureUncertain = values[2];
        vulnerabilityUncertain = values[3];
        consequenceUncertain = values[4];
        symptomUncertain = values[5];
    }

    public double getLossUncertain() {
        return lossUncertain;
    }

    public void setLossUncertain(double lossUncertain) {
        this.lossUncertain = lossUncertain;
    }

    public double getThreatUncertain() {
        return threatUncertain;
    }

    public void setThreatUncertain(double threatUncertain) {
        this.threatUncertain = threatUncertain;
    }

    public double getMeasureUncertain() {
        return measureUncertain;
    }

    public void setMeasureUncertain(double measureUncertain) {
        this.measureUncertain = measureUncertain;
    }

    public double getVulnerabilityUncertain() {
        return vulnerabilityUncertain;
    }

    public void setVulnerabilityUncertain(double vulnerabilityUncertain) {
        this.vulnerabilityUncertain = vulnerabilityUncertain;
    }

    public double getConsequenceUncertain() {
        return consequenceUncertain;
    }

    public void setConsequenceUncertain(double consequenceUncertain) {
        this.consequenceUncertain = consequenceUncertain;
    }

    public double getSymptomUncertain() {
        return symptomUncertain;
    }

    public void setSymptomUncertain(double symptomUncertain) {
        this.symptomUncertain = symptomUncertain;
    }
}
