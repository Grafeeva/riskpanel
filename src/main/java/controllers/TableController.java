package controllers;

import com.sun.javafx.sg.prism.NGShape;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.ObservableSet;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Side;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.chart.PieChart;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.stage.DirectoryChooser;
import javafx.util.Pair;
import model.Model;
import model.TableData;
import view.App;

import java.io.File;
import java.net.URL;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

/**
 */
public class TableController implements Initializable {
    @FXML
    public PieChart pie_chartLoss;

    @FXML
    public Button btn;
    @FXML
    public PieChart pie_chartThreat;
    @FXML
    public PieChart pie_chartMeasure;
    @FXML
    public PieChart pie_chartVulnerability;
    @FXML
    public PieChart pie_chartSymptom;
    @FXML
    public PieChart pie_chartConsequence;

    @FXML
    public BarChart chart;

    @FXML
    public TableColumn<TableData, Number> symptom;
    @FXML
    public TableView<TableData> tableView;
    @FXML
    public TableColumn<TableData, String> precedentName;
    @FXML
    public TableColumn<TableData, Number> loss;
    @FXML
    public TableColumn<TableData, Number> threat;
    @FXML
    public TableColumn<TableData, Number> measure;
    @FXML
    public TableColumn<TableData, Number> vulnerability;
    @FXML
    public TableColumn<TableData, Number> consequence;
    @FXML
    public MenuItem item_menu_close;
    @FXML
    public MenuItem item_menu_open;
    @FXML
    public Label countPrecedent;
    @FXML
    public Label countPrecedentWithoutRelationships;
    @FXML
    public Label resultDBUncertainLabel;
    @Override
    public void initialize(URL location, ResourceBundle resources) {


        precedentName.setCellValueFactory(cellData -> cellData.getValue().precedentNameProperty());
        symptom.setCellValueFactory(cellData -> cellData.getValue().symptomUncertainProperty());
        loss.setCellValueFactory(cellData -> cellData.getValue().lossUncertainProperty());
        threat.setCellValueFactory(cellData -> cellData.getValue().threatUncertainProperty());
        measure.setCellValueFactory(cellData -> cellData.getValue().measureUncertainProperty());
        vulnerability.setCellValueFactory(cellData -> cellData.getValue().vulnerabilityUncertainProperty());
        consequence.setCellValueFactory(cellData -> cellData.getValue().consequenceUncertainProperty());
        tableView.setItems(Model.getInstance().getTableData());

        double loss = 0;
        double threat = 0;
        double measure = 0;
        double vulnerability = 0;
        double consequence = 0;
        double symptom = 0;

        ObservableList<TableData> data = Model.getInstance().getTableData();
        for (TableData cell : data) {
            loss += cell.getLossUncertain();
            threat += cell.getThreatUncertain();
            measure += cell.getMeasureUncertain();
            vulnerability += cell.getVulnerabilityUncertain();
            consequence += cell.getConsequenceUncertain();
            symptom += cell.getSymptomUncertain();
        }
        loss /= data.size();
        loss *= 100;

        threat /= data.size();
        threat *= 100;

        measure /= data.size();
        measure *= 100;

        vulnerability /= data.size();
        vulnerability *= 100;

        consequence /= data.size();
        consequence *= 100;

        symptom /= data.size();
        symptom = symptom * 100;

        ObservableList<PieChart.Data> pieChartData1 =
                FXCollections.observableArrayList(
                        new PieChart.Data("Неопределенность", loss),
                        new PieChart.Data("Определенность", 100 - loss)
                        );

        pie_chartLoss.setData(pieChartData1);
        ObservableList<PieChart.Data> pieChartData2 =
                FXCollections.observableArrayList(
                        new PieChart.Data("Неопределенность", threat),
                        new PieChart.Data("Определенность", 100 - threat)
                );

        pie_chartThreat.setData(pieChartData2);

        ObservableList<PieChart.Data> pieChartData3 =
                FXCollections.observableArrayList(
                        new PieChart.Data("Неопределенность", measure),
                        new PieChart.Data("Определенность", 100 - measure)
                );

        pie_chartMeasure.setData(pieChartData3);
        ObservableList<PieChart.Data> pieChartData4 =
                FXCollections.observableArrayList(
                        new PieChart.Data("Неопределенность", vulnerability),
                        new PieChart.Data("Определенность", 100 - vulnerability)
                );

        pie_chartVulnerability.setData(pieChartData4);
        ObservableList<PieChart.Data> pieChartData5 =
                FXCollections.observableArrayList(
                        new PieChart.Data("Неопределенность", consequence),
                        new PieChart.Data("Определенность", 100 - consequence)
                );

        pie_chartConsequence.setData(pieChartData5);
        ObservableList<PieChart.Data> pieChartData6 =
                FXCollections.observableArrayList(
                        new PieChart.Data("Неопределенность", symptom),
                        new PieChart.Data("Определенность", 100 - symptom)
                );

        pie_chartSymptom.setData(pieChartData6);

        final Label caption = new Label("");
        caption.setTextFill(Color.DARKBLUE);
        caption.setStyle("-fx-font: 24 arial;");

        pie_chartMeasure.setLabelLineLength(10);
        pie_chartMeasure.setLegendSide(Side.LEFT);

        pie_chartSymptom.setLabelLineLength(10);
        pie_chartSymptom.setLegendSide(Side.LEFT);

        pie_chartConsequence.setLabelLineLength(10);
        pie_chartConsequence.setLegendSide(Side.LEFT);

        pie_chartVulnerability.setLabelLineLength(10);
        pie_chartVulnerability.setLegendSide(Side.LEFT);

        pie_chartThreat.setLabelLineLength(10);
        pie_chartThreat.setLegendSide(Side.LEFT);

        pie_chartLoss.setLabelLineLength(10);
        pie_chartLoss.setLabelsVisible(false);
        pie_chartLoss.setLegendSide(Side.LEFT);

        pie_chartMeasure.setClockwise(false);

        for (final PieChart.Data data1 : pie_chartMeasure.getData()) {
            data1.getNode().addEventHandler(MouseEvent.MOUSE_CLICKED,
                    e -> {
                        System.out.println(String.valueOf(data1.getPieValue()));
                        caption.setTranslateX(e.getSceneX());
                        caption.setTranslateY(e.getSceneY());
                        caption.setText(String.valueOf(data1.getPieValue()) + "%");
                    });
        }

        Map<TableData, Integer> equals = Model.getInstance().getEqualsCount();

        List<Pair<String, Integer>> list =  equals.entrySet().stream()
                .map((a) -> new Pair<String, Integer>(String.format("%.10s - %d", a.getKey().getPrecedentName(), a.getValue()), a.getValue()))
                .sorted((a, b) -> -a.getValue().compareTo(b.getValue()))
                .limit(10).collect(Collectors.toList());



        double result = (symptom + loss + threat + consequence + measure + vulnerability) / 6.0;
        resultDBUncertainLabel.textProperty().setValue(Double.toString(result));

        XYChart.Series series1 = new XYChart.Series();
        series1.setName("1");

        for (Pair<String, Integer> i : list) {
            series1.getData().add(new XYChart.Data(i.getKey(), i.getValue()));
        }

        chart.getData().addAll(series1);
        countPrecedent.textProperty().bind(Model.getInstance().countPrecedentPropertyProperty());
        countPrecedentWithoutRelationships.textProperty().bind(Model.getInstance().countPrecedentWithoutRelationshipsPropertyProperty());


    }

    public void closeDB() throws Exception {
        Model.getInstance().closeConnection();
        App.getInstance().goToStartPage();
    }

    public void openDB() throws Exception {
        DirectoryChooser directoryChooser = new DirectoryChooser();
        directoryChooser.setTitle("Open database folder");
        directoryChooser.setInitialDirectory(new File(Model.DEFAULT_PATH));
        File file = directoryChooser.showDialog(null);
        if(file != null) {
            Model.getInstance().connectToDB(file.getPath());
        }
        App.getInstance().goToDatabasePage();

    }


    @FXML
    public void next(ActionEvent event) {
        App.getInstance().goToRemovePrecedentPage(event); //todo

    }
}




