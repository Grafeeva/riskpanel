package controllers;

import javafx.application.Platform;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.fxml.FXML;

import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.stage.Stage;
import javafx.util.Callback;
import model.Model;
import model.Precedent;
import model.TableData;
import view.App;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.stream.Collector;
import java.util.stream.Collectors;

/**
 * Created by Kate on 03.05.2016.
 */
public class RemovePrecedentController implements Initializable {
    @FXML
    public TableView<Precedent> removedPrecedentTableView;

    @FXML
    public TableColumn<Precedent, String> precedentName;

    @FXML
    public TableColumn<Precedent, Boolean> checkBox;

    @FXML
    public Button backButton;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        checkBox.setCellFactory( tc -> new CheckBoxTableCell<>());
        checkBox.setCellValueFactory( cellData -> cellData.getValue().isRemovedProperty());
        checkBox.setEditable(true);
        checkBox.setOnEditCommit(new EventHandler<TableColumn.CellEditEvent<Precedent, Boolean>>() {
            @Override
            public void handle(TableColumn.CellEditEvent<Precedent, Boolean> event) {
                event.getRowValue().setRemoved(event.getNewValue());
            }
        });

        precedentName.setCellValueFactory(cellData -> cellData.getValue().getNameProperty());

        removedPrecedentTableView.setItems(Model.getInstance().getEmptyPrecedents());

        removedPrecedentTableView.setEditable(true);

    }

    @FXML
    public void analyze() {
        Stage stage = (Stage) backButton.getScene().getWindow();
        stage.close();
        Model.getInstance().removedPrecedents = Model.getInstance().getEmptyPrecedents().stream().filter(Precedent::isRemoved).collect(Collectors.toList());

        Model.getInstance().clear();
        App.getInstance().goToConnectPage();
        new Thread(new Runnable() {
            @Override
            public void run() {
                Controller.startAnalysis();
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        App.getInstance().goToDatabasePage(); //todo
                    }
                });
            }
        }).start();

    }

    @FXML
    public void selectAll() {
        for (Precedent precedent : Model.getInstance().getEmptyPrecedents()) {
            precedent.setRemoved(true);
        }
    }

    @FXML
    public void disableAll() {
        for (Precedent precedent : Model.getInstance().getEmptyPrecedents()) {
            precedent.setRemoved(false);
        }
    }

    @FXML
    public void back() {
        Stage stage = (Stage) backButton.getScene().getWindow();
        stage.close();
    }
}
