package model;

import javafx.beans.property.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class TableData {
    private StringProperty precedentName;

    private DoubleProperty lossUncertain;
    private DoubleProperty threatUncertain;
    private DoubleProperty measureUncertain;
    private DoubleProperty vulnerabilityUncertain;
    private DoubleProperty consequenceUncertain;
    private DoubleProperty symptomUncertain;

    private List<Tree> trees;

    public TableData(String precedentName, Map<String, Double> map) {
        this.precedentName = new SimpleStringProperty(precedentName);

        for(String label : map.keySet()) {
            if(label.equals(TreeLabel.LOSS.toString())) this.lossUncertain = new SimpleDoubleProperty(map.get(label));
            if(label.equals(TreeLabel.THREAT.toString())) this.threatUncertain = new SimpleDoubleProperty(map.get(label));
            if(label.equals(TreeLabel.CONSEQUENCE.toString())) this.consequenceUncertain = new SimpleDoubleProperty(map.get(label));
            if(label.equals(TreeLabel.SYMPTOM.toString())) this.symptomUncertain = new SimpleDoubleProperty(map.get(label));
            if(label.equals(TreeLabel.VULNERABILITY.toString())) this.vulnerabilityUncertain = new SimpleDoubleProperty(map.get(label));
            if(label.equals(TreeLabel.MEASURE.toString())) this.measureUncertain = new SimpleDoubleProperty(map.get(label));
        }
    }

    public List<Tree> getTrees() {
        return trees;
    }

    public void setTrees(List<Tree> trees) {
        this.trees = (trees.stream().map(Tree::new).collect(Collectors.toList()));
    }

    public String getPrecedentName() {
        return precedentName.get();
    }

    public StringProperty precedentNameProperty() {
        return precedentName;
    }

    public double getLossUncertain() {
        return lossUncertain.get();
    }

    public DoubleProperty lossUncertainProperty() {
        return lossUncertain;
    }

    public double getThreatUncertain() {
        return threatUncertain.get();
    }

    public DoubleProperty threatUncertainProperty() {
        return threatUncertain;
    }

    public double getMeasureUncertain() {
        return measureUncertain.get();
    }

    public DoubleProperty measureUncertainProperty() {
        return measureUncertain;
    }

    public double getVulnerabilityUncertain() {
        return vulnerabilityUncertain.get();
    }

    public DoubleProperty vulnerabilityUncertainProperty() {
        return vulnerabilityUncertain;
    }

    public double getConsequenceUncertain() {
        return consequenceUncertain.get();
    }

    public DoubleProperty consequenceUncertainProperty() {
        return consequenceUncertain;
    }

    public double getSymptomUncertain() {
        return symptomUncertain.get();
    }

    public DoubleProperty symptomUncertainProperty() {
        return symptomUncertain;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TableData tableData = (TableData) o;

        //if (!precedentName.equals(tableData.precedentName)) return false;
        if (lossUncertain.doubleValue() != tableData.lossUncertain.doubleValue()) return false;
        if (threatUncertain.doubleValue() != tableData.threatUncertain.doubleValue()) return false;
        if (measureUncertain.doubleValue() != tableData.measureUncertain.doubleValue()) return false;
        if (vulnerabilityUncertain.doubleValue() != tableData.vulnerabilityUncertain.doubleValue()) return false;
        if (consequenceUncertain.doubleValue()  != tableData.consequenceUncertain.doubleValue()) return false;
        if (symptomUncertain.doubleValue() != tableData.symptomUncertain.doubleValue()) return false;


        return trees.equals(tableData.trees);

    }

    @Override
    public int hashCode() {
        int result = 0;
        result = 31 * result + Double.hashCode(lossUncertain.doubleValue());
        result = 31 * result + Double.hashCode(threatUncertain.doubleValue());
        result = 31 * result + Double.hashCode(measureUncertain.doubleValue());
        result = 31 * result + Double.hashCode(vulnerabilityUncertain.doubleValue());
        result = 31 * result + Double.hashCode(consequenceUncertain.doubleValue());
        result = 31 * result + Double.hashCode(symptomUncertain.doubleValue());
        //result = 31 * result + trees.hashCode();
        return result;
    }
}
