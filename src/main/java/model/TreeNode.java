package model;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class TreeNode {
    public static final int UNCERTAIN = 0;
    public static final int POSITIVE = 1;
    public static final int NEGATIVE = -1;

    private int id;
    private int value;
    private double deep;

    private List<TreeNode> children;
    private TreeNode parent;

    public TreeNode(int id, TreeNode parent, double deep) {
        value = UNCERTAIN;
        this.id = id;
        this.deep = deep;
        this.parent = parent;
        children = new ArrayList<>();
    }

    public TreeNode(TreeNode node, TreeNode parent) {
        id = node.id;
        value = node.value;
        deep = node.deep;
        this.parent = parent;

        children = (node.children.stream().map(child -> new TreeNode(child, this)).collect(Collectors.toList()));
    }

    public int getId() {
        return id;
    }

    public int getValue() {
        return value;
    }

    public double getDeep() {
        return deep;
    }

    public List<TreeNode> getChildren() {
        return children;
    }

    public TreeNode getParent() {
        return parent;
    }

    public TreeNode getNode(int id) {
        if (id == this.id) return this;
        for (TreeNode node : children) {
            TreeNode find = node.getNode(id);
            if (find != null) return find;
        }
        return null;
    }

    public void setValue(int value) {
        if (value == POSITIVE || this.value == UNCERTAIN) {
            this.value = value;
        }
        if (value == POSITIVE && parent != null) {
            List<TreeNode> siblings = parent.getChildren();
            for (TreeNode sibling: siblings) {
                sibling.setValue(NEGATIVE);

            }
            parent.setValue(value);
        }
//
//        if (value == POSITIVE) {
//            if (parent != null) parent.setValue(POSITIVE);
//        }

    }

    public void addChild(TreeNode node) {
        children.add(node);
    }

    public void clean() {
        value = UNCERTAIN;
        for (TreeNode child : children) {
            child.clean();
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TreeNode treeNode = (TreeNode) o;

        if (id != treeNode.id) return false;
        if (value != treeNode.value) return false;
        if (Double.compare(treeNode.deep, deep) != 0) return false;
        if (treeNode.children.size() != children.size()) return false;
        List<TreeNode> temp = new ArrayList<>(treeNode.children);
        for (TreeNode node:children) {
            boolean exist = temp.remove(node);
            if (!exist) {
                return false;
            }
        }
        return true;//parent.equals(treeNode.parent);

    }

    @Override
    public int hashCode() {
        int result = 0;
        long temp;
        result = id;
        result = 31 * result + value;
        temp = Double.doubleToLongBits(deep);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + children.hashCode();
//        result = 31 * result + parent.hashCode();
        return result;
    }
}
