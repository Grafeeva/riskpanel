package model;

import javafx.beans.property.*;

public class Precedent {

    private String name;

    private StringProperty nameProperty;
    private int id;
    private Boolean empty;
    private BooleanProperty removed;

    public Precedent(String name, int id) {
        this.name = name;
        this.id = id;
        this.empty = false;
        this.nameProperty = new SimpleStringProperty(name);
        removed = new SimpleBooleanProperty(false);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Boolean getEmpty() {
        return empty;
    }

    public void setEmpty(Boolean empty) {
        this.empty = empty;
    }

    public boolean isRemoved() {
        return removed.get();
    }

    public BooleanProperty isRemovedProperty() {
        return removed;
    }

    public StringProperty getNameProperty() {
        return nameProperty;
    }

    public void setRemoved(boolean removed) {
        this.removed.set(removed);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Precedent precedent = (Precedent) o;

        if (id != precedent.id) return false;
        return !(name != null ? !name.equals(precedent.name) : precedent.name != null);

    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + id;
        return result;
    }
}
