package view;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.fxml.JavaFXBuilderFactory;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;

/**
 */
public class App extends Application {
    public static final String APP_TITLE = "Analisys knowledge base";

    private Stage stage;
    private Scene scene;

    private static App instance ;


    public App() {
        instance = this;
    }

    public static App getInstance() {
        return instance;
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        stage = primaryStage;
        stage.setTitle(APP_TITLE);

        goToStartPage();
        stage.show();
    }

    public void goToStartPage() {
        replaceSceneContent("start_view.fxml");
    }

    public void goToDatabasePage() {
        replaceSceneContent("db_result.fxml");
    }

    public void goToConnectPage() {
        replaceSceneContent("connect_to_db_view.fxml");
    }

    private Parent replaceSceneContent(String fxml) {
        URL url = App.class.getResource(fxml);
        Parent page = null;
        try {
            page = (Parent) FXMLLoader.load(url, null, new JavaFXBuilderFactory());

            if (scene == null) {
                scene = new Scene(page);
                stage.setScene(scene);
            } else {
                scene.setRoot(page);
            }
            stage.sizeToScene();

        } catch (IOException e) {
            e.getLocalizedMessage();
        }
        return page;
    }

    public void  goToRemovePrecedentPage(ActionEvent event) {
        URL url = App.class.getResource("removePrecedent.fxml");
        Parent page = null;

        try {
            page = (Parent) FXMLLoader.load(url, null, new JavaFXBuilderFactory());
        } catch (IOException e) {
            e.printStackTrace();
        }
       // stage.setScene(new Scene(page));
        Stage curStage = new Stage();
        curStage.initModality(Modality.WINDOW_MODAL);
        curStage.initOwner(
               stage);

        Scene dialogScene =new Scene(page);
        curStage.setScene(dialogScene);
        curStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }

    public Scene getScene() {
        return scene;
    }

    public Stage getStage() {
        return stage;
    }
}
