package model;

import org.neo4j.graphdb.RelationshipType;

public enum Relation implements RelationshipType {
    HAS_DETAIL, IS_KIND_OF
}