package model.clustering;

import model.TableData;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kate on 10.11.2015.
 */
public class Cluster {
    private List<TableData> data = new ArrayList<>();
    private Center center;

    public Cluster(Center center) {
        this.center = center;
    }

    public void addPrecedent(TableData precedent) {
        if (!data.contains(precedent)) {
            data.add(precedent);
        }
    }

    public Center getCenter() {
        return center;
    }

    public void removePrecedent(TableData precedent) {
        if(data.contains(precedent)) data.remove(precedent);
    }

    public List<TableData> getData() {
        return data;
    }

    public void setCenter(Center center) {
        this.center = center;
    }

    public boolean contains(TableData data) {
        return this.data.contains(data);
    }
}
