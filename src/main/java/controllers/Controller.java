package controllers;

import model.*;
import model.clustering.KMeans;
import org.neo4j.graphdb.*;
import org.neo4j.helpers.collection.IteratorUtil;

import java.util.*;

public class Controller {
    private static GraphDatabaseService db;

    public static void startAnalysis() {

        createTrees();
        calc();
        KMeans kMeans = new KMeans(8, Model.getInstance().getTableData());
        kMeans.print();
    }

    private static void createTrees() {
        db = Model.getInstance().getDbService();
        TreeLabel[] labels = TreeLabel.values();
        for (TreeLabel label : labels) {
            Tree tree = getTree(label.toString());
            Model.getInstance().addTreeToAttr(tree);
        }
    }

    private static Tree getTree(String treeName) {
        Tree tree = null;
        try (Transaction ignored = db.beginTx()) {
            Result result = db.execute(String.format("MATCH (root:%s {id: 0}) RETURN root", treeName));
            Iterator<Node> column = result.columnAs("root");
            while (column.hasNext()) {
                if (tree == null) {
                    tree = createTree(column, db, treeName);
                }
            }
        }
        return tree;
    }

    private static Tree createTree(Iterator<Node> column, GraphDatabaseService db, String label) {
        double deep = 0.0;

        Node node = column.next();
        TreeNode root = new TreeNode((Integer) node.getProperty("id"), null, deep);
        Tree tree = new Tree(root, label);

        Queue<TreeNode> queue = new LinkedList<>();
        queue.add(root);
        while (!queue.isEmpty()) {
            TreeNode nod = queue.poll();
            Result result = db.execute(String.format("MATCH (node: %s {id: %d})<-[:%s]-(child) RETURN child", label, nod.getId(), Relation.IS_KIND_OF.name()));
            Iterator<Node> child_column = result.columnAs("child");
            while (child_column.hasNext()) {
                for (Node n : IteratorUtil.asIterable(child_column)) {
                    if (deep < nod.getDeep()) {
                        deep = nod.getDeep();
                    }
                    TreeNode child = new TreeNode((Integer) n.getProperty("id"), nod, deep + 1);
                    nod.addChild(child);
                    queue.add(child);
                }
            }
        }
        tree.setDeep(deep + 1);
        return tree;
    }

    private static void calc() {
        try (Transaction ignored = db.beginTx()) {
            Result result = db.execute("MATCH (n:Precedent) RETURN n");
            Iterator<Node> n_column = result.columnAs("n");
            for (Node node : IteratorUtil.asIterable(n_column)) {
                Precedent precedent = new Precedent(node.getProperty("name").toString(), (Integer) node.getProperty("id"));
                if (Model.getInstance().removedPrecedents.contains(precedent)) {
                    continue;
                }
                Model.getInstance().countPrecedentInc();

                fillTree(node, precedent);

                calculateUncertainForPrecedent(precedent, Model.getInstance().getAttributes());

                Model.getInstance().getAttributes().forEach(model.Tree::cleanTree);
            }
        }
    }


    private static void fillTree(Node node, Precedent precedent) {
        List<Tree> trees = Model.getInstance().getAttributes();

        Iterable<Relationship> r = node.getRelationships(Relation.HAS_DETAIL, Direction.OUTGOING);

        if (!r.iterator().hasNext()) {
            precedent.setEmpty(true);
            Model.getInstance().countPrecedentWithoutRelationshipsInc(precedent);
        }
        for (Relationship relationship : r) {
            Node node1 = relationship.getEndNode();
            for (Label label : IteratorUtil.asIterable(node1.getLabels().iterator())) {
                for (Tree tree : trees) {
                    if (label.name().equals(tree.getLabel())) {
                        Integer i = (Integer) node1.getProperty("id");
                        TreeNode node2 = tree.getNodeById(i);
                        if (node2 != null) node2.setValue(TreeNode.POSITIVE);
                    }
                }
            }
        }

        //trees.forEach(model.Tree::fillUncertain);

    }

    private static void calculateUncertainForPrecedent(Precedent precedent, List<Tree> trees) {
//        for (Tree tree : trees) {
//            tree.setUncertainty(tree.getRoot().getValue() == TreeNode.NEGATIVE ? 0.0 : 1.0);
//        }

        Queue<TreeNode> queue = new LinkedList<>();

        for (Tree tree : trees) {
           // if (tree.getUncertainty() == 1.0) {
                queue.add(tree.getRoot());
                tree.setUncertainty(calculateUncertain(queue, tree.getDeep()));
          //  }
        }

        Map<String, Double> uncertainMap = new HashMap<>();

        for (Tree tree : trees) {
            uncertainMap.put(tree.getLabel(), tree.getUncertainty());
        }
        TableData data = new TableData(precedent.getName(), uncertainMap);
        data.setTrees(trees);
        Model.getInstance().addItem(data);

    }

    private static double calculateUncertain(Queue<TreeNode> queue, double maxDeep) {
        while (!queue.isEmpty()) {
            TreeNode node = queue.poll();
            TreeNode parent = node.getParent();

            if (node.getValue() == TreeNode.UNCERTAIN) {
                if (parent == null) {
                    return 1.0;
                }
                return 1.0 - (parent.getDeep() / maxDeep);
            }


            for (TreeNode node1 : node.getChildren()) queue.add(node1);
        }
        return 0.0;
    }
}
