package model;

import org.omg.CORBA.TRANSACTION_MODE;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class Tree {
    private String label;
    private double deep;
    private double uncertainty;

    private TreeNode root;

    public Tree(TreeNode root, String label) {
        this.root = root;
        this.label = label;
    }

    public Tree(Tree tree) {

        label = tree.getLabel();
        deep = tree.getDeep();
        uncertainty = tree.getUncertainty();

        root = new TreeNode(tree.getRoot(), null);
    }

    public String getLabel() {
        return label;
    }

    public double getDeep() {
        return deep;
    }

    public double getUncertainty() {
        return uncertainty;
    }

    public TreeNode getRoot() {
        return root;
    }

    public TreeNode getNodeById(int id) {
        return root.getNode(id);
    }

    public void setDeep(double deep) {
        this.deep = deep;
    }

    public void setUncertainty(double uncertainty) {
        this.uncertainty = uncertainty;
    }

    public void cleanTree() {
        root.clean();
    }

    public void fillUncertain() {

        Queue<TreeNode> queue = new LinkedList<>();
        queue.add(root);

        while (!queue.isEmpty()) {
            TreeNode node = queue.poll();
            if (node.getValue() == TreeNode.UNCERTAIN) {
                for (TreeNode child : node.getChildren()) child.setValue(TreeNode.UNCERTAIN);
            }
            double deep = node.getDeep() + 1;
            List<TreeNode> nodes = getNodesByDeep(deep);

            boolean change = true;

            for (TreeNode child : nodes) {
                if (child.getValue() == TreeNode.POSITIVE) change = false;
            }
            for (TreeNode child : node.getChildren()) {
                if (child.getValue() == TreeNode.NEGATIVE && node.getValue() == TreeNode.POSITIVE && change) {
                    child.setValue(TreeNode.UNCERTAIN);
                }
            }
            for(TreeNode child : node.getChildren()) queue.add(child);
        }


    }

    private List<TreeNode> getNodesByDeep(double deep) {
        Queue<TreeNode> queue = new LinkedList<>();
        queue.add(root);
        List<TreeNode> nodes = new ArrayList<>();

        while(!queue.isEmpty()) {
            TreeNode node = queue.poll();
            if(node.getDeep() == deep) nodes.add(node);
            for(TreeNode child : node.getChildren()) queue.add(child);
        }
        return nodes;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Tree tree = (Tree) o;

        if (Double.compare(tree.deep, deep) != 0) return false;
        if (Double.compare(tree.uncertainty, uncertainty) != 0) return false;
        return root.equals(tree.root);

    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = label.hashCode();
        temp = Double.doubleToLongBits(deep);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(uncertainty);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + root.hashCode();
        return result;
    }
}
