package controllers;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;
import javafx.stage.DirectoryChooser;
import model.Model;
import view.App;

import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;

public class ChooseDBController implements Initializable {
    @FXML
    private TextField path;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        path.setText(Model.DEFAULT_PATH);
    }

    @FXML
    public void connectToDB() throws Exception {
        App.getInstance().goToConnectPage();
        new Thread(new Runnable() {
            @Override
            public void run() {
                Model.getInstance().connectToDB(path.getText());
                Controller.startAnalysis();
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        App.getInstance().goToDatabasePage(); //todo
                    }
                });
            }
        }).start();

    }

    @FXML
    public void choosePathToDB() {
        DirectoryChooser directoryChooser = new DirectoryChooser();
        directoryChooser.setTitle("Open database folder");
        directoryChooser.setInitialDirectory(new File(path.getText()));
        File file = directoryChooser.showDialog(null);
        if (file != null) {
            path.setText(file.getPath());
        }
    }
}
