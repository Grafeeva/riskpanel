package model;

public enum TreeLabel {
    LOSS, THREAT, SYMPTOM, MEASURE, CONSEQUENCE, VULNERABILITY;

    @Override
    public String toString() {
        switch (this) {
            case LOSS: return "Loss";
            case THREAT: return "Threat";
            case SYMPTOM: return "Symptom";
            case MEASURE: return "Measure";
            case CONSEQUENCE: return "Consequence";
            case VULNERABILITY: return "Vulnerability";
            default: return super.toString();
        }
    }
}